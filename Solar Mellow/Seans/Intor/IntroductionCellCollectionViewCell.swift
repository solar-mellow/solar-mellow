//
//  IntroductionCellCollectionViewCell.swift
//  Solar Mellow
//
//  Created by Sara Mady on 03/10/2021.
//

import UIKit

class IntroductionCellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var introImage: UIImageView!
    @IBOutlet weak var introTitle: UILabel!
    @IBOutlet weak var introSubTitle: UILabel!
    
}
