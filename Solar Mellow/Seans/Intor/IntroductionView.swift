//
//  IntroductionView.swift
//  Solar Mellow
//
//  Created by Sara Mady on 03/10/2021.
//

import UIKit

class IntroductionView: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    var imageArray = [#imageLiteral(resourceName: "intro1"),#imageLiteral(resourceName: "intro4"),#imageLiteral(resourceName: "intro3")]
    var MainTitles = ["Title 1","Title 2","Title 3"]
    var SubTitle = ["Sub 1" , "Sub 2" , "Sub 3"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }

    
    @IBAction func currentUserAction(_ sender: Any) {
    }
    
    
    @IBAction func newUserAction(_ sender: Any) {
        let vc = Storyboard.Main.viewController(cheackLocationViewController.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageController.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)

    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageController.currentPage = indexPath.section
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroductionCellCollectionViewCell", for: indexPath) as! IntroductionCellCollectionViewCell
        
        cell.introImage.image =  imageArray[indexPath.row]
        cell.introTitle.text = MainTitles[indexPath.row]
        cell.introSubTitle.text = SubTitle[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width:collectionView.frame.width , height: collectionView.frame.height)

       
    }
    
    
}

