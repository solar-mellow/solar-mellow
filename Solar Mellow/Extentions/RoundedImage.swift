//
//  RoundedImage.swift
//  Ra3ia
//
//  Created by Sara Mady on 24/03/2021.
//

import Foundation
import UIKit


extension UIImageView {

    func MakeRounded() {

        self.layer.borderWidth = 0.7
        self.layer.masksToBounds = false
        self.layer.borderColor = #colorLiteral(red: 0.8855473399, green: 0.5395184755, blue: 0.06974243373, alpha: 1)
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
  
    
}
