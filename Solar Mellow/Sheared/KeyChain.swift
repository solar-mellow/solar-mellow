//
//  KeyChain.swift
//  RPTClient
//
//  Created by Abdallah Nader on 2/16/20.
//  Copyright © 2020 aait. All rights reserved.
//
//
//import Foundation
//import KeychainSwift
//
//struct KeyChain {
//    private static let tokenKey = "token"
//    static var userToken: String? {
//        get {
//            let keychain = KeychainSwift()
//            return keychain.get(tokenKey)
//        } set {
//            let keychain = KeychainSwift()
//            if let value = newValue {
//                keychain.set(value, forKey: tokenKey, withAccess: .accessibleAfterFirstUnlockThisDeviceOnly)
//            } else {
//                keychain.delete(tokenKey)
//            }
//        }
//    }
//
//    static var tokenExist: Bool {
//        let keychain = KeychainSwift()
//        return keychain.get(tokenKey) != nil
//    }
//
//    static func deleteToken() {
//        let keychain = KeychainSwift()
//        keychain.delete(tokenKey)
//    }
//
//    private static let sessionIdKey = "sessionId"
//    static var sessionId: String? {
//        get {
//            let keychain = KeychainSwift()
//            return keychain.get(sessionIdKey)
//        } set {
//            let keychain = KeychainSwift()
//            if let value = newValue {
////                keychain.set(value, forKey: sessionIdKey)
//                keychain.set(value, forKey: sessionIdKey, withAccess: .accessibleAfterFirstUnlockThisDeviceOnly)
//            } else {
//                keychain.delete(sessionIdKey)
//            }
//        }
//    }
//
//    private static let sessionTokenKey = "sessionToken"
//    static var sessionToken: String? {
//        get {
//            let keychain = KeychainSwift()
//            return keychain.get(sessionTokenKey)
//        } set {
//            let keychain = KeychainSwift()
//            if let value = newValue {
//                keychain.set(value, forKey: sessionTokenKey, withAccess: .accessibleAfterFirstUnlockThisDeviceOnly)
//            } else {
//                keychain.delete(sessionTokenKey)
//            }
//        }
//    }
//
//    private static let openTokApiKey = "openTokApi"
//    static var openTokApi: String? {
//        get {
//            let keychain = KeychainSwift()
//            return keychain.get(openTokApiKey)
//        } set {
//            let keychain = KeychainSwift()
//            if let value = newValue {
//                keychain.set(value, forKey: openTokApiKey, withAccess: .accessibleAfterFirstUnlockThisDeviceOnly)
//            } else {
//                keychain.delete(openTokApiKey)
//            }
//        }
//    }
//
//
//    private static let callerIdKey = "callerIdKey"
//    static var callerId: String? {
//        get {
//            let keychain = KeychainSwift()
//            return keychain.get(callerIdKey)
//        } set {
//            let keychain = KeychainSwift()
//            if let value = newValue {
//                keychain.set(value, forKey: callerIdKey, withAccess: .accessibleAfterFirstUnlockThisDeviceOnly)
//            } else {
//                keychain.delete(callerIdKey)
//            }
//        }
//    }
//
//    private static let callerNameKey = "callerNameKey"
//    static var callerName: String? {
//        get {
//            let keychain = KeychainSwift()
//            return keychain.get(callerNameKey)
//        } set {
//            let keychain = KeychainSwift()
//            if let value = newValue {
//                keychain.set(value, forKey: callerNameKey, withAccess: .accessibleAfterFirstUnlockThisDeviceOnly)
//            } else {
//                keychain.delete(callerNameKey)
//            }
//        }
//    }
//
//    private static let callerAvatarKey = "callerAvatarKey"
//    static var callerAvatar: String? {
//        get {
//            let keychain = KeychainSwift()
//            return keychain.get(callerAvatarKey)
//        } set {
//            let keychain = KeychainSwift()
//            if let value = newValue {
//                keychain.set(value, forKey: callerAvatarKey, withAccess: .accessibleAfterFirstUnlockThisDeviceOnly)
//            } else {
//                keychain.delete(callerAvatarKey)
//            }
//        }
//    }
//
//}
