//
//  AppAlert.swift
//  SeaTrips
//
//  Created by Sara Mady on 11/4/20.
//  Copyright © 2020 Sara Ashraf. All rights reserved.
//

import Foundation
import UIKit
import SPAlert

  public  func showSuccessAlert(title:String,message:String){
        SPAlert.present(title: message, preset: .done)
    }
    
public func showErrorAlert(title:String,message:String){
        SPAlert.present(title: message, preset: .error)

    }

func completData(title:String,message:String){
    SPAlert.present(message:message, haptic: .error)

}

func showNoInterNetAlert(){
    SPAlert.present(title: "Please make sure you are connected to the internet".localized, preset: .error)

}
func showLikedAlert(msg:String){
    SPAlert.present(title: msg, preset: .heart)

}
func showWarningAlert(title:String,message:String){
    SPAlert.present(title: message, preset: .custom(UIImage(named: "cirrcle")!))
    
    
}
