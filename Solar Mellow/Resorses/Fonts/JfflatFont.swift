//
//  JfflatFont.swift
//  Sandkom
//
//  Created by Sara Ashraf on 1/16/20.
//  Copyright © 2020 Sara Ashraf. All rights reserved.
//

import Foundation

enum CairoFont: String, AppFont {
    case light = "Cairo-Light"
    case regular = "Cairo-Regular"
    case semiBold = "Cairo-SemiBold"
//    case bold = "Cairo-Bold"
//    case black = "Cairo-Black"

}
